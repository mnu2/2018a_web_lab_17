<%@ page import="java.time.LocalDateTime" %>
<%@ page import="java.time.DayOfWeek" %>
<%@ page import="com.sun.org.glassfish.gmbal.IncludeSubclass" %><%--
  Created by IntelliJ IDEA.
  User: mnu2
  Date: 15/05/2018
  Time: 11:27 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Exercise 01-01</title>
    </head>
    <body>
        <h1>Date Time Display</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean facilisis ex et urna posuere porttitor. Mauris et justo a massa laoreet molestie non nec felis. Sed id pellentesque turpis. Ut scelerisque erat ut laoreet scelerisque. Duis condimentum lacinia lorem ut pellentesque. Nulla consectetur gravida risus eu pharetra. Curabitur iaculis faucibus euismod. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean ullamcorper mauris orci, id ullamcorper elit ultrices sed. Donec est neque, auctor eu nunc vel, congue aliquet ipsum. In sit amet feugiat magna. Mauris mattis malesuada eros, id luctus nisl. Vivamus eu rhoncus augue. Donec viverra, mi nec convallis fringilla, nulla lectus vestibulum justo, eu venenatis nunc justo eu nunc. Vestibulum ut neque quis quam sodales finibus. Aenean pulvinar, felis sit amet luctus mattis, nunc justo porta nibh, id convallis lorem mi in orci.</p><hr>
        <%@include file="exercise02-date.jsp" %>
    </body>
</html>
