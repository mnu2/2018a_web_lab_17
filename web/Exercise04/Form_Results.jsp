<%@ page import="java.util.Map" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.security.PublicKey" %><%--
  Created by IntelliJ IDEA.
  User: mnu2
  Date: 15/05/2018
  Time: 1:20 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Form Results</title>
    </head>
    <body>
        <%	Map<String, String[]> map = request.getParameterMap();
            Iterator<Map.Entry<String, String[]>> i = map.entrySet().iterator();%>
            <table>
            <% while(i.hasNext()) {

                Map.Entry<String, String[]> entry = i.next();
                String key = entry.getKey(); //.toUpperCase();
                String[] values = entry.getValue();

                if(key.contains("submit") || key.contains("button")) {
                    continue;
                }

                int index = key.indexOf("[]");
                if(index != -1) {
                    key = key.substring(0, index);
                }%>
                <tr><td>
                <%=key + ": "%>
                </td>
                    <%
                for(String value: values) {%>
                    <td>
                    <%="&lt;" + value + "&gt;"%>
                    </td></tr>
                <%}%>

                <%}%>
            </table>
    </body>
</html>
