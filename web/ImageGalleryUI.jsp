<%--
  Created by IntelliJ IDEA.
  User: mnu2
  Date: 15/05/2018
  Time: 2:20 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <title>Animals of the World</title>
    </head>
    <body>
        <h1>Check Out These Animals, Yo!</h1>
        <table>
            <header>
                <th><a href=<%="?sortColumn=filesize&order=" + session.getAttribute("filesizeSortToggle") + "ending"%>>Filename <img src=<%="/Photos/sort-" + session.getAttribute("currFilesizeSortToggle") + ".png' alt='icon'"%> /></a></th>
                <th><a href=<%="?sortColumn=filename&order=" + session.getAttribute("filenameSortToggle") + "ending"%>>Thumbnail <img src=<%="/Photos/sort-" + session.getAttribute("currFilenameSortToggle") + ".png' alt='icon'"%> /></a></th></header>
            <c:forEach items="${requestScope.get('photoData')}" var="image" >
                <tr>
                    <td>${image.getFullfileSize()} bytes </td>
                    <td><a href="${image.getFullFile()}"><img src="${image.getThumbPath()}"></a></td>
                </tr>
            </c:forEach>
        </table>
    </body>
</html>
