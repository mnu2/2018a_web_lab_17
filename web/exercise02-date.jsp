<%@ page import="java.time.LocalDateTime" %>
<%@ page import="java.time.DayOfWeek" %><%--
  Created by IntelliJ IDEA.
  User: mnu2
  Date: 15/05/2018
  Time: 12:43 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Date File</title>
    </head>
    <body>
        <% LocalDateTime dateTime = LocalDateTime.now(); DayOfWeek day = dateTime.getDayOfWeek();
            String hour = "" + dateTime.getHour(); String minute = "" + dateTime.getMinute(); String second = "" + dateTime.getSecond();
        if (hour.length() == 1){hour = "0" + hour;}
        else if(hour.length() == 0){hour = "00";}
            if (minute.length() == 1){minute = "0" + minute;}
            else if(minute.length() == 0){minute = "00";}
            if (second.length() == 1){second = "0" + second;}
            else if(second.length() == 0){second = "00";}

        %>
        <p><%=day%> <%=hour%>:<%=minute%>:<%=second%></p>
    </body>
</html>
